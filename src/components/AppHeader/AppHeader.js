import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Tooltip } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  titleLogo: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
    textAlign: 'right',
  },
  logo: {
    width: 120,
  },
  link: { color: 'white', textDecoration: 'none' },
}));

function AppHeader(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="subtitle1" className={classes.titleLogo}>
            <i className={'fab fa-react fa-2x'}></i>
          </Typography>
          <Typography variant="subtitle1" className={classes.title}>
            <Tooltip title={'view code source'}>
              <a
                className={classes.link}
                href={'https://gitlab.com/flavio.espinoza/react-redux-data-table'}
                target="_blank"
                rel="noreferrer"
                style={{ color: 'white', textDecoration: 'none' }}>
                <i className="fab fa-gitlab"></i>
                <span className={'ml12'}>{props.title}</span>
              </a>
            </Tooltip>
          </Typography>
          <Typography variant="subtitle1">
            <a
              className={classes.link + ' ml12'}
              href={'https://www.loom.com/share/f452b19f90b746a3856d54af2e504323'}
              target="_blank"
              rel="noreferrer">
              <Tooltip title={'watch how to video'}>
                <i className="fad fa-question-circle"></i>
              </Tooltip>
            </a>
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default AppHeader;
