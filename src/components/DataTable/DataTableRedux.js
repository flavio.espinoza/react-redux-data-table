import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  clearData,
  fetchDataAsync,
  selectError,
  selectSuccess,
  selectEntities,
  selectGridData,
} from './dataTableSlice.js';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Link } from '@material-ui/core';
import { TextField, Button } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { XGrid } from '@material-ui/x-grid';
import ReactJson from 'react-json-view';
import CsvDownload from 'react-json-to-csv';
import buttonPrimaryStyle from '../../assets/jss/button-primary';

const useStyles = makeStyles((theme) => ({
  tableRoot: {
    width: '100%',
  },
  paper: {
    paddingTop: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    paddingBottom: theme.spacing(4),
    textAlign: 'left',
    color: theme.palette.text.secondary,
    overflow: 'hidden',
    minHeight: 760,
  },
  dense: {
    marginTop: theme.spacing(2),
  },
  xGridContainer: { height: 620, width: '100%' },
}));

function DataTableRedux() {
  const classes = useStyles();
  const data = useSelector(selectGridData);
  const dispatch = useDispatch();
  const success = useSelector(selectSuccess);
  const error = useSelector(selectError);
  const entities = useSelector(selectEntities);
  const [url, setUrl] = useState('');

  const handleChange = (e, stateProp) => {
    if (!stateProp || stateProp === '') {
      alert('second argument `stateProp` must be a non-empty string value');
      return;
    }
    if (stateProp === 'url') {
      setUrl(e.target.value);
    }
  };

  const useTestApi = () => {
    setUrl('https://jsonplaceholder.typicode.com/posts');
  };

  return (
    <Paper className={classes.paper + ' mt12 pb12'}>
      <div className="flex-container">
        <small className="flex-1 pl6 mt6">
          <Link href={'#'} color={'primary'} onClick={useTestApi}>
            Click to use test API
          </Link>
        </small>
      </div>
      <div className={'flex-container'}>
        <div className={'flex-1'}>
          <TextField
            label="api url"
            className={classes.dense}
            fullWidth
            margin="dense"
            variant="outlined"
            onChange={(e) => handleChange(e, 'url')}
            value={url}
          />
        </div>
        <div className={''}>
          <Button
            className={`${classes.dense} ml6`}
            variant="contained"
            color="primary"
            disabled={url === ''}
            onClick={() => dispatch(fetchDataAsync(url))}>
            Get Data
          </Button>
          <Button
            className={`${classes.dense} ml6`}
            variant="contained"
            color="secondary"
            disabled={entities.length <= 0}
            onClick={() => {
              dispatch(clearData());
              setUrl('');
            }}>
            Clear Data
          </Button>
        </div>
      </div>
      {success && data.rows.length > 0 ? (
        <div className={classes.xGridContainer}>
          <CsvDownload data={data.rows} style={buttonPrimaryStyle} />
          <XGrid {...data} rowHeight={40} />
        </div>
      ) : null}
      {!success ? (
        <Alert severity={'error'}>
          <ReactJson
            style={{
              padding: 12,
            }}
            theme={'monokai'}
            src={error}
          />
        </Alert>
      ) : null}
    </Paper>
  );
}

export default DataTableRedux;
