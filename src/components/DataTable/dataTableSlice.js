import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const mapDataToGrid = (data) => {
  return {
    columns: [
      {
        field: 'userId',
        headerName: 'User ID',
        width: 100,
        type: 'number',
      },
      {
        field: 'id',
        headerName: 'Post ID',
        width: 100,
        type: 'number',
      },
      {
        field: 'title',
        headerName: 'Title',
        width: 400,
      },
      {
        field: 'body',
        headerName: 'Body',
        width: 1000,
      },
    ],
    rows: data,
  };
};

export const fetchDataAsync = createAsyncThunk('data/fetchData', async (url, thunkAPI) => {
  try {
    const res = await axios.get(`${url}`);
    if (res.data && Array.isArray(res.data)) {
      return {
        success: true,
        data: res.data,
        url: url,
        error: {},
      };
    } else {
      return {
        success: false,
        data: [],
        url: url,
        error: {
          message: 'Error: res.data is not an Array of Objects',
          api: url,
          res: {
            data: res.data,
          },
        },
      };
    }
  } catch (err) {
    const error = new Error(err.message);
    return {
      success: false,
      data: [],
      url: url,
      error: {
        message: `Error: ${error.message}`,
        api: url,
      },
    };
  }
});

export const initialState = {
  success: true,
  entities: [],
  gridData: {
    columns: [],
    rows: [],
  },
  error: {},
  entitiesApi: '',
}

const dataSlice = createSlice({
  name: 'data',
  initialState: initialState,
  reducers: {
    clearData: (state, action) => {
      state.entities = [];
      state.entitiesApi = '';
      state.gridData = {
        columns: [],
        rows: [],
      };
    },
  },
  extraReducers: {
    [fetchDataAsync.fulfilled]: (state, action) => {
      if (!action.payload.success) {
        state.success = false;
        state.error = action.payload.error;
        state.entitiesApi = '';
        state.entities = [];
        state.gridData = {
          columns: [],
          rows: [],
        };
      } else {
        const mapGridData = mapDataToGrid(action.payload.data);
        state.success = true;
        state.entitiesApi = action.payload.url;
        state.entities = [...action.payload.data];
        state.gridData = {
          columns: [...mapGridData.columns],
          rows: [...mapGridData.rows],
        };
      }
    },
  },
});

export const { clearData } = dataSlice.actions;

export const selectError = (state) => {
  return state.data.error;
};

export const selectSuccess = (state) => {
  return state.data.success;
};

export const selectApi = (state) => {
  return state.data.entitiesApi;
};

export const selectEntities = (state) => {
  return state.data.entities;
};

export const selectGridData = (state) => {
  return state.data.gridData;
};

// async reducer for DataTableHooks.js
async function asyncReducer(state, action) {
  switch (action.type) {
    case 'fetchDataAsyncReact':
      return {
        ...state,
        success: action.payload.success,
        entities: [...action.payload.data],
        gridData: {
          columns: [...action.payload.gridData.columns],
          rows: [...action.payload.gridData.rows],
        },
        entitiesApi: action.payload.url,
        error: { ...action.payload.error },
      };
    case 'clearDataReact':
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

const reducers = {
  data: dataSlice.reducer,
  async: asyncReducer,
};

export default reducers;
