import React, { useState } from 'react';
import axios from 'axios'
import {
  initialState,
  mapDataToGrid,
} from './dataTableSlice.js';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Link } from '@material-ui/core';
import { TextField, Button } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { XGrid } from '@material-ui/x-grid';
import ReactJson from 'react-json-view';
import CsvDownload from 'react-json-to-csv';
import buttonPrimaryStyle from '../../assets/jss/button-primary'

const useStyles = makeStyles((theme) => ({
  tableRoot: {
    width: '100%',
  },
  paper: {
    paddingTop: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    paddingBottom: theme.spacing(4),
    textAlign: 'left',
    color: theme.palette.text.secondary,
    overflow: 'hidden',
    minHeight: 760,
  },
  dense: {
    marginTop: theme.spacing(2),
  },
}));

function DataTableHooks({reactState, reactDispatchState}) {
  const classes = useStyles();
  const [url, setUrl] = useState('');

  const handleChange = (e, stateProp) => {
    if (!stateProp || stateProp === '') {
      alert('second argument `stateProp` must be a non-empty string value');
      return;
    }
    if (stateProp === 'url') {
      setUrl(e.target.value);
    }
  };

  const useTestApi = () => {
    setUrl('https://jsonplaceholder.typicode.com/posts');
  };

  const asyncFetchData = async (apiUrl) => {
    try {
      const res = await axios.get(apiUrl);
      return {
        success: true,
        data: res.data,
        gridData: mapDataToGrid(res.data),
        url: apiUrl,
        error: {}
      }
    } catch(err) {
      const error = new Error(err.message);
      return {
        success: false,
        data: [],
        gridData: {
          columns: [],
          rows: [],
        },
        url: apiUrl,
        error: {
          message: `Error: ${error.message}`,
          api: url,
        },
      };
    }
  }

  return (
    <Paper className={classes.paper + ' mt12 pb12'}>
      <div className="flex-container">
        <small className="flex-1 pl6 mt6">
          <Link href={'#'} color={'primary'} onClick={useTestApi}>
            Click to use test API
          </Link>
        </small>
      </div>
      <div className={'flex-container'}>
        <div className={'flex-1'}>
          <TextField
            label="api url"
            className={classes.dense}
            fullWidth
            margin="dense"
            variant="outlined"
            onChange={(e) => handleChange(e, 'url')}
            value={url}
          />
        </div>
        <div className={''}>
          <Button
            className={`${classes.dense} ml6`}
            variant="contained"
            color="primary"
            disabled={url === ''}
            onClick={async () => {
              const payload = await asyncFetchData(url)
              reactDispatchState({type: 'fetchDataAsyncReact', payload: payload})
            }}>
            Get Data
          </Button>
          <Button
            className={`${classes.dense} ml6`}
            variant="contained"
            color="secondary"
            disabled={reactState.entities.length <= 0}
            onClick={() => {
              setUrl('');
              reactDispatchState({type: 'clearDataReact', payload: initialState})
            }}>
            Clear Data
          </Button>
        </div>
      </div>
      {reactState.success && reactState.gridData.rows.length > 0 ? (
        <div style={{ height: 620, width: '100%' }}>
          <CsvDownload
            data={reactState.gridData.rows}
            style={buttonPrimaryStyle}
          />
          <XGrid {...reactState.gridData} rowHeight={40} />
        </div>
      ) : null}
      {!reactState.success ? (
        <Alert severity={'error'}>
          <ReactJson
            style={{
              padding: 12,
            }}
            theme={'monokai'}
            src={reactState.error}
          />
        </Alert>
      ) : null}
    </Paper>
  );
}

export default DataTableHooks;
