import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import AppHeader from './components/AppHeader/AppHeader';
import DataTableRedux from './components/DataTable/DataTableRedux';
import DataTableHooks from './components/DataTable/DataTableHooks';
import logo from './assets/img/logo--react-white.png';
import reducers, { initialState } from './components/DataTable/dataTableSlice';
import useAsyncReducer from './hooks/useAsyncReducer';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));

function App() {
  const classes = useStyles();
  const [reactState, reactDispatchState] = useAsyncReducer(reducers.async, initialState);
  return (
    <div className={classes.root}>
      <AppHeader
        {...{
          title: 'React Redux Data Table',
        }}
      />
      <Grid container spacing={1} justify="center">
        <Grid item lg={10} xs={12}>
          <Typography className={'p12'} variant={'h4'}>
            DataTableRedux.js
          </Typography>
          <Typography className={'pl12'} variant={'body2'}>
            Uses Redux. Manages state with React-Redux hooks{' '}
            <a
              href="https://react-redux.js.org/api/hooks#useselector"
              target="_blank"
              rel="noreferrer">
              <b>useSelector</b>
            </a>{' '}
            and
            <a
              href="https://react-redux.js.org/api/hooks#usedispatch"
              target="_blank"
              rel="noreferrer">
              <b>useDispatch</b>
            </a>{' '}
          </Typography>
          <DataTableRedux />
        </Grid>
        <Grid item lg={10} xs={12}>
          <Typography className={'p12'} variant={'h4'}>
            DataTableHooks.js
          </Typography>
          <Typography className={'pl12'} variant={'body2'}>
            Does not use Redux. Manages state with custom hook <b>useAsyncReducer</b> with custom
            reducer <b>asyncReducer</b>
          </Typography>
          <DataTableHooks
            {...{
              reactState: reactState,
              reactDispatchState: reactDispatchState,
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
