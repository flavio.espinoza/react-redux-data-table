import { useState } from 'react';

export default function useAsyncReducer(reducer, initialState) {
  const [state, setState] = useState(initialState);
  async function dispatchState(action) {
    const nextState = await reducer(state, action);
    setState(nextState)
  }
  return [state, dispatchState];
}
