import { configureStore } from '@reduxjs/toolkit';
import reducers from '../components/DataTable/dataTableSlice';

export const store = configureStore({
  reducer: {
    data: reducers.data,
  },
});
