# react-redux-data-table

Datable using React and Redux with the following capabilities:

1. A text box to enter the JSON API URL and import button (you can use any fake API from <https://jsonplaceholder.typicode.com/>)

2. Ability to import JSON and display rows/cols

3. Option to export data as CSV file

4. Search box - Searches across rows/columns - then filter the data in the table

## Live Demo

Deployed on Digital Ocean: https://react-redux-data-table-rdxja.ondigitalocean.app

How To: [Video](https://www.loom.com/share/f452b19f90b746a3856d54af2e504323)

## Getting Started

### Clone Repo

```shell
git clone https://gitlab.com/flavio.espinoza/react-redux-data-table.git
```

### Install

CD into the project directory and install dependencies:

```shell
yarn install
```

### Start

```shell
yarn start
```

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Build

```shell
yarn build
```

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### Deploy

Deployed using [Digital Ocean Apps Platform](https://www.digitalocean.com/products/app-platform/)

### Tech

[Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

[Material-UI](https://material-ui.com/)

[Font Awesome](https://fontawesome.com/)
